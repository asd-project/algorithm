//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

namespace asd
{
    template <class T = int>
    struct counting_generator
    {
        constexpr counting_generator(T value = 0) noexcept :
            value(value) {}

        constexpr T operator() () noexcept {
            return value++;
        }

        T value;
    };

    counting_generator() -> counting_generator<int>;

    template <class T>
    counting_generator(T value) -> counting_generator<T>;
}
