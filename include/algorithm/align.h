//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <type_traits>

//---------------------------------------------------------------------------

namespace asd
{
    template <class X, class A>
    constexpr auto align(X x, A a) {
        return ((x - 1) | (a - 1)) + 1;
    }

    template <class X, class Y, class A, class R = std::common_type_t<X, Y, A>>
    constexpr R aligned_add(X x, Y y, A a) {
        R v = static_cast<R>(x) + y + a - 1;
        return v - v % a;
    }
}
