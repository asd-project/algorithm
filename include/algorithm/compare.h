//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

namespace asd
{
    template <class T, class Y>
    constexpr int compare(T x, Y y) noexcept {
        return x > y ? 1 : x < y ? -1 : 0;
    }

    template <class T, class Y>
    constexpr int icomp(T x, Y y) noexcept {
        return x >= y ? 1 : -1;
    }

    template <class T>
    constexpr bool between(T value, T min, T max) {
        return value >= min && value < max;
    }
}
