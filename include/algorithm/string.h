//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <string>
#include <string_view>

#include <meta/macro.h>
#include <algorithm/compare.h>

//---------------------------------------------------------------------------

namespace asd
{
    constexpr char ascii_tolower(char c) noexcept {
        return c >= 'A' && c <= 'Z' ? c + ('a' - 'A') : c;
    }

    constexpr int case_insensitive_compare(char a, char b) noexcept {
        a = ascii_tolower(a);
        b = ascii_tolower(b);

        return a < b ? -1 : a > b ? 1 : 0;
    }

    template<class InputIt1, class InputIt2, class Comp>
    constexpr int lexicographical_compare(InputIt1 first1, InputIt1 last1, InputIt2 first2, InputIt2 last2, Comp compare) noexcept {
        for (; first1 != last1 && first2 != last2; first1++, first2++) {
            auto c = compare(*first1, *first2);

            if (c == 0) {
                continue;
            }

            return c;
        }

        return first2 != last2 ? -1 : first1 != last1 ? 1 : 0;
    }

    template<class Comp>
    constexpr int lexicographical_compare(const char * a, const char * b, Comp compare) noexcept {
        for (; *a && *b; a++, b++) {
            auto c = compare(*a, *b);

            if (c == 0) {
                continue;
            }

            return c;
        }

        return *a ? -1 : *b ? 1 : 0;
    }

    constexpr int lexicographical_compare(const char * a, const char * b) noexcept {
        for (; *a && *b; a++, b++) {
            auto c = compare(*a, *b);

            if (c == 0) {
                continue;
            }

            return c;
        }

        return *a ? -1 : *b ? 1 : 0;
    }

    struct case_insensitive_less
    {
        template <class T1, class T2>
        constexpr bool operator()(const T1 & a, const T2 & b) const noexcept {
            return asd::lexicographical_compare(std::begin(a), std::end(a), std::begin(b), std::end(b), case_insensitive_compare) < 0;
        }
    };

    // STL doesn't have it for a certain reason, but we can add it painlessly
    // http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2013/n3512.html#lib-modifications
    // https://stackoverflow.com/questions/44636549/why-is-there-no-support-for-concatenating-stdstring-and-stdstring-view

    inline std::string operator + (const std::string & a, std::string_view b) {
        std::string s(a);
        s.append(b);

        return s;
    }

    inline std::string operator + (std::string && a, std::string_view b) {
        a.append(b);
        return std::move(a);
    }
}
